package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DateUtil {
	
	private static final String DATE_FORMAT = "dd/M/yy";
	
	public static Date getDateFromString(String dateString) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date date = sdf.parse(dateString); 
		return date;
	}

	public static String getStringFromDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		String dateString = sdf.format(date); 
		return dateString;
	}
}
