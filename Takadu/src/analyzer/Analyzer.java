package analyzer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import util.DateUtil;
import model.SupplyValue;
import model.SupplyZone;
import filereader.Readable;
import filereader.SupplyValuesFileReader;
import filereader.SupplyZoneFileReader;
import filereader.exception.WaterNetworkException;



public class Analyzer {
	
	public void analyze(String zoneFilePath, String valueFilePath, Date[] reportPeriod, String outputPath) throws WaterNetworkException, ParseException, IOException{
		
		SupplyZoneFileReader zoneReader = new SupplyZoneFileReader();
		Map<String, Readable> zonesMap = zoneReader.readFile(zoneFilePath);
		SupplyValuesFileReader valueReader = new SupplyValuesFileReader();
		valueReader.readFile(valueFilePath);
		File file = new File(outputPath);
	    file.createNewFile();
	    FileWriter writer = new FileWriter(file); 
	    
	    writer.write("Zone Name,Date, Value, Value Type\n");
	    
	    for(String zoneName: zonesMap.keySet()){
	    	SupplyZone zone = (SupplyZone) zonesMap.get(zoneName);
	    	for(Date dateKey: reportPeriod){
	    		SupplyValue value = zone.getValueByDate(dateKey);
	    		if(value != null){
		    		writer.write(zone.getZoneName() + ",");
		    		writer.write(DateUtil.getStringFromDate(dateKey)+ ",");
		    		writer.write(value.getSupplyValue()+ ",");
		    		writer.write(value.getValueType().toString());
		    		writer.write("\n");
	    		}
	    	}
	    }
	    writer.flush();
	    writer.close();
	}
}
