package model;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.util.SupplyValueType;
import filereader.Readable;


public class SupplyZone implements Readable {

	private String zoneName;
	private SupplyZone parentZone;
	private List<SupplyZone> childs;
	private Map<Date, SupplyValue> values;
	

	public SupplyZone(String zoneName, SupplyZone parentZone) {
		this.zoneName = zoneName;
		this.parentZone = parentZone;
		values = new HashMap<Date, SupplyValue>();
	}
	
	public SupplyZone(){
		values = new HashMap<Date, SupplyValue>();
	}
	
	public SupplyZone(String zoneName) {
		this.zoneName = zoneName;
		values = new HashMap<Date, SupplyValue>();
	}

	@Override
	public String getZoneName() {
		return zoneName;
	}
	
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	public SupplyZone getParentZone() {
		return parentZone;
	}
	
	public void setParentZone(SupplyZone parentZone) {
		this.parentZone = parentZone;
	}
	
	@Override
	public int hashCode() {
		
		int hash=7;
		for (int i=0; i < zoneName.length(); i++) {
		    hash = hash * 31 + zoneName.charAt(i);
		}
		return hash;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj == null){
			return false;
		}
		
		if(this == obj){
			return true;
		}
		
		if(!(obj instanceof SupplyZone)){
			return false;
		}
						
		SupplyZone supplyZone = (SupplyZone)obj;
		if(this.parentZone == supplyZone.parentZone && this.zoneName == supplyZone.zoneName){
			return true;				
		}
	
		return false;
	}
	
	protected void addChild(SupplyZone zone){
		if(childs == null){
			childs = new ArrayList<SupplyZone>();
		}
		if(!childs.contains(zone)){
			childs.add(zone);
		}
	}
	
	public List<SupplyZone> getChilds(){
		return childs;
	}
	
	public SupplyZone getChildByName(String childName){
		for(SupplyZone child: childs){
			if(child.getZoneName().equals(childName)){
				return child;
			}
		}
		return null;
	}

	public void addValue(SupplyValue value) {
		if(values == null){
			values = new HashMap<Date, SupplyValue>();
		}
		value.setValueType(SupplyValueType.ACTUAL);
		if(!values.containsKey(value.getDate())){
			values.put(value.getDate(), value);
		}
		
		
	}
	
	public boolean hasValues(){
		return values != null && values.size() > 0;
	}
	
	public SupplyValue getValueByDate(Date date){
		if(values == null){
			return null;
		}	
		
		SupplyValue supplyValueByDate = values.get(date);
		if(supplyValueByDate == null && hasChildren()){
			supplyValueByDate = calculateValueFromChilds(supplyValueByDate, date);
		}
		
		return supplyValueByDate;
	}
	
	private SupplyValue calculateValueFromChilds(SupplyValue supplyValue, Date date){
		
			int sum = 0;
			for(SupplyZone child: childs){
				if(child.getValueByDate(date) != null){
					sum += child.getValueByDate(date).getSupplyValue();
				}								
			}
			supplyValue = new SupplyValue();
			supplyValue.setDate(date);
			supplyValue.setSupplyValue(sum);
			supplyValue.setZone(this);
			supplyValue.setValueType(SupplyValueType.AGGREGATED);
			values.put(date, supplyValue);
		
		
		return supplyValue;
	}
	
	public boolean hasChildren(){
		return childs != null && childs.size() > 0;
	}
		
}
