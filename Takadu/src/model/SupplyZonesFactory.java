package model;

import java.util.HashMap;
import java.util.Map;

import filereader.exception.WaterNetworkException;

public class SupplyZonesFactory {

	private static Map<String, SupplyZone> supplyZoneMap;
		
	public static void resetSupplyZoneMap(){
		supplyZoneMap = null;
	}
	
	public static SupplyZone createSupplyZone(String zoneName, String parentName){
		
		if(supplyZoneMap == null)
			supplyZoneMap = new HashMap<String, SupplyZone>();
		
		SupplyZone zone = retrieveZone(zoneName);
		if(parentName != null){
			SupplyZone parentZone = retrieveZone(parentName);
			zone.setParentZone(parentZone);
			parentZone.addChild(zone);
		}
					
		return zone;
	}

	private static SupplyZone retrieveZone(String zoneName) {
		SupplyZone zone;
		if(supplyZoneMap.containsKey(zoneName)){
			zone = supplyZoneMap.get(zoneName);
		} else {
			zone = new SupplyZone(zoneName);
			supplyZoneMap.put(zoneName, zone);
		}
		return zone;
		
	}

	public static SupplyZone getSupplyZone(String zoneName) throws WaterNetworkException {
		
		if(supplyZoneMap == null){
			throw new WaterNetworkException("No zones were found");
		}
		
		SupplyZone zoneFromMap = supplyZoneMap.get(zoneName);
		if(zoneFromMap != null){
			return zoneFromMap;
		}
		
		throw new WaterNetworkException("Zone " + zoneName + " was not found");
	}
}
