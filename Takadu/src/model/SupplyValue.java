package model;


import java.util.Date;

import model.util.SupplyValueType;
import filereader.Readable;

public class SupplyValue implements Readable {

	
	private SupplyZone zone;
	private Date date;
	private int supplyValue;
	private SupplyValueType valueType;
	
	public SupplyZone getZone() {
		return zone;
	}
	
	public void setZone(SupplyZone zone) {
		this.zone = zone;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getSupplyValue() {
		return supplyValue;
	}
	
	public void setSupplyValue(int supplyValue) {
		this.supplyValue = supplyValue;
	}
		
	@Override
	public String getZoneName() {
		return zone.getZoneName();
	}
	
	
	public SupplyValueType getValueType() {
		return valueType;
	}

	public void setValueType(SupplyValueType valueType) {
		this.valueType = valueType;
	}
	
}
