package filereader;

import model.SupplyValue;
import model.SupplyZone;
import model.SupplyZonesFactory;
import util.DateUtil;

public class SupplyValuesFileReader extends AbstractWaterNetworkFileReader {

	@Override
	protected Readable getLineContent(String line) throws Exception {
		
		String[] contentLine = line.split(CVS_SEPERARTOR);
		if(contentLine.length == 0){
			return null;
		} else {
			String zoneName = contentLine[0];
			int supplyValue = Integer.parseInt(contentLine[2]);
			SupplyValue value = new SupplyValue();
			SupplyZone zone = SupplyZonesFactory.getSupplyZone(zoneName);
			value.setZone(zone);
			value.setDate(DateUtil.getDateFromString(contentLine[1]));
			value.setSupplyValue(supplyValue);
			zone.addValue(value);
			return value;
		}		
			
	}

}
