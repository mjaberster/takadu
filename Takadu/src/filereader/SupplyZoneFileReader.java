package filereader;

import model.SupplyZone;
import model.SupplyZonesFactory;



public class SupplyZoneFileReader extends AbstractWaterNetworkFileReader {

	public SupplyZoneFileReader(){
		SupplyZonesFactory.resetSupplyZoneMap();
	}
			
	@Override
	protected Readable getLineContent(String line) {
		
		String[] contentLine = line.split(CVS_SEPERARTOR);
		SupplyZone zone;
		if(contentLine !=null && contentLine.length == 0){
			return null;
		} else {
			String zoneName = contentLine[0];
			String parentZone = null;
			if(contentLine.length > 1){
				parentZone = contentLine[1];
			}
			
			zone = SupplyZonesFactory.createSupplyZone(zoneName, parentZone);
			return zone;
		}
			
	}
	
}
