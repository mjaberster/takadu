package filereader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import filereader.exception.WaterNetworkException;


public abstract class AbstractWaterNetworkFileReader {

	
	protected static final String CVS_SEPERARTOR = ",";

	public Map<String, Readable> readFile(String csvFile) throws WaterNetworkException{
			
			BufferedReader br = null;
			String line = "";
		 
			try {
				Map<String, Readable> fileContent = new HashMap<String, Readable>();
				br = new BufferedReader(new FileReader(csvFile));
				if((line = br.readLine()) != null){
					while ((line = br.readLine()) != null) {
						Readable content = getLineContent(line);
						fileContent.put(content.getZoneName(), content);
					}
					return fileContent;
				}
				
				throw new WaterNetworkException("File " + csvFile + " was empty");
		 
			} catch (Exception e) {
				throw new WaterNetworkException(e);
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	
	protected abstract Readable getLineContent(String line) throws Exception;
}
