package filereader.exception;


public class WaterNetworkException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WaterNetworkException(String exceptionMessage) {
		super(exceptionMessage);
	}

	public WaterNetworkException(Exception e) {
		super(e);
	}

}
