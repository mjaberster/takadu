package takadu.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import takadu.test.testcase.SupplyValuesTestCase;
import takadu.test.testcase.SupplyZoneTestCase;

@RunWith(value=Suite.class)
@SuiteClasses(value={
					SupplyZoneTestCase.class,
					SupplyValuesTestCase.class
})

public class MainTestSuite  {
	
	
}
