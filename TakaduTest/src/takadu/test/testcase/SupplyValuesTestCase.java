package takadu.test.testcase;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import model.SupplyValue;
import model.SupplyZone;

import org.junit.Assert;
import org.junit.Test;

import takadu.test.testcase.expected.WaterNetworkOutputCSVReader;
import util.DateUtil;
import analyzer.Analyzer;
import filereader.Readable;
import filereader.SupplyValuesFileReader;
import filereader.SupplyZoneFileReader;
import filereader.exception.WaterNetworkException;

public class SupplyValuesTestCase {

	private static final String SUPPLY_VALUE_FILE_PATH = "c:/temp/takadu/SupplyValues.csv";
	private static final String SUPPLY_ZONE_FILE_PATH = "c:/temp/takadu/SupplyZones.csv";
	
	@Test
	public void testValueForZone() throws WaterNetworkException, ParseException{
		
		SupplyZoneFileReader zoneReader = new SupplyZoneFileReader();
		Map<String, Readable> zonesMap = zoneReader.readFile(SUPPLY_ZONE_FILE_PATH);
		SupplyValuesFileReader valueReader = new SupplyValuesFileReader();
		valueReader.readFile(SUPPLY_VALUE_FILE_PATH);
		
		SupplyZone givataimZone = (SupplyZone) zonesMap.get("Givataim");
		SupplyValue value = givataimZone.getValueByDate(DateUtil.getDateFromString("1/1/2014"));
		Assert.assertEquals(10, value.getSupplyValue());
		
		SupplyZone tacenterZone = (SupplyZone) zonesMap.get("TA Center");
		value = tacenterZone.getValueByDate(DateUtil.getDateFromString("1/1/2014"));
		Assert.assertEquals(4, value.getSupplyValue());
		
		SupplyZone gushDanZone = (SupplyZone) zonesMap.get("Gush Dan");
		Assert.assertFalse(gushDanZone.hasValues());
		
	}

	@Test
	public void testGeneratedOutput() throws WaterNetworkException, ParseException, IOException{
		
		Analyzer analyzer = new Analyzer();
		
		Date[] reportPeriod = new Date[7];
		reportPeriod[0] = DateUtil.getDateFromString("1/1/2014");
		reportPeriod[1] = DateUtil.getDateFromString("2/1/2014");
		reportPeriod[2] = DateUtil.getDateFromString("3/1/2014");
		reportPeriod[3] = DateUtil.getDateFromString("4/1/2014");
		reportPeriod[4] = DateUtil.getDateFromString("5/1/2014");
		reportPeriod[5] = DateUtil.getDateFromString("6/1/2014");
		reportPeriod[6] = DateUtil.getDateFromString("7/1/2014");
		
		String outputPath = "c:/temp/takadu/output_" + System.currentTimeMillis() + ".csv";
		analyzer.analyze(SUPPLY_ZONE_FILE_PATH, SUPPLY_VALUE_FILE_PATH,reportPeriod ,outputPath);
		
		WaterNetworkOutputCSVReader outputCSVReader = new WaterNetworkOutputCSVReader();
		File file = new File(getClass().getResource("expected/resource/expected.csv").getFile());
		Map<String, Readable> expectedCsvMap = outputCSVReader.readFile(file);
		Map<String, Readable> outputCsvMap = outputCSVReader.readFile(outputPath);
		
		Collection<Readable> expectedValues = expectedCsvMap.values();
		Collection<Readable> outputValues = outputCsvMap.values();
	    
		Assert.assertEquals(expectedValues.size(), outputValues.size());
		Assert.assertTrue(expectedValues.containsAll(outputValues));
		Assert.assertTrue(outputValues.containsAll(expectedValues));		
		
	}
	
}
