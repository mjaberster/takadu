package takadu.test.testcase.expected;

import java.io.File;
import java.util.Map;

import filereader.AbstractWaterNetworkFileReader;
import filereader.Readable;
import filereader.exception.WaterNetworkException;

public class WaterNetworkOutputCSVReader extends AbstractWaterNetworkFileReader {

	@Override
	protected Readable getLineContent(String line) throws Exception {
		
		String[] contentLine = line.split(CVS_SEPERARTOR);
		if(contentLine.length == 0){
			return null;
		} else {
			String zoneName = contentLine[0];
			String dateString = contentLine[1];
			int supplyValue = Integer.parseInt(contentLine[2]);
			String valueType = contentLine[3];
			OutputCSV csv = new OutputCSV();
			csv.setZoneName(zoneName);
			csv.setDate(dateString);
			csv.setValue(supplyValue);
			csv.setValueType(valueType);
			return csv;
		}
	}

	public Map<String, Readable> readFile(File file) throws WaterNetworkException {
		return super.readFile(file.getAbsolutePath());
	}

}
