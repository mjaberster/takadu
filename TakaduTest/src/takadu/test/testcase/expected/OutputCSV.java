package takadu.test.testcase.expected;

import filereader.Readable;

public class OutputCSV implements Readable {

	
	String zoneName;
	String date;
	int value;
	String valueType;
	
	@Override
	public String getZoneName() {
		return zoneName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj == this){
			return true;
		} else if(obj instanceof OutputCSV){
			OutputCSV csv = (OutputCSV)obj;
			if(this.getZoneName().equals(csv.getZoneName()) && 
					this.getValue() == csv.getValue() && 
					this.getValueType().equals(csv.getValueType())){
				return true;
			}
		} else {
			return false;
		}
		return super.equals(obj);
	}
	

}
