package takadu.test.testcase;

import java.util.Map;

import model.SupplyZone;

import org.junit.Assert;
import org.junit.Test;

import filereader.Readable;
import filereader.SupplyZoneFileReader;
import filereader.exception.WaterNetworkException;

public class SupplyZoneTestCase {

	private static final String FILE_PATH = "c:/temp/takadu/SupplyZones.csv";
	
	@Test
	public void testZonesNoDuplicate() throws WaterNetworkException{
		
		SupplyZoneFileReader reader = new SupplyZoneFileReader();
		Map<String, Readable> zonesMap = reader.readFile(FILE_PATH);
		SupplyZone gushDanZone = (SupplyZone)zonesMap.get("Gush Dan");
		SupplyZone tlvZone = (SupplyZone)zonesMap.get("Tel Aviv");
		Assert.assertEquals(gushDanZone, tlvZone.getParentZone());
		
	}
	
	@Test
	public void testChilds() throws WaterNetworkException{
		SupplyZoneFileReader reader = new SupplyZoneFileReader();
		Map<String, Readable> zonesMap = reader.readFile(FILE_PATH);
		SupplyZone gushDanZone = (SupplyZone)zonesMap.get("Gush Dan");
		Assert.assertEquals(3, gushDanZone.getChilds().size());
		SupplyZone tlv = gushDanZone.getChildByName("Tel Aviv");
		Assert.assertEquals(4, tlv.getChilds().size());
	}
	
	
}
